#!/bin/bash

# Checking overall storage utilization
df -TH && df -iTH

# Configuring repositories
echo "Configuring repositories..."
dnf install 'dnf-command(config-manager)' -y
yum config-manager --set-enabled powertools

# Downloading epel-release and Development Tools
echo "Downloading epel-release and Development Tools..."
yum install epel-release -y
yum groupinstall "Development Tools" -y

# Checking storage utilization
df -TH && df -iTH

# Downloading dependencies
echo ".........................Downloading dependencies.................................................."
yum install audit-libs-devel binutils-devel elfutils-devel kabi-dw ncurses-devel newt-devel \
numactl-devel openssl-devel pciutils-devel perl perl-devel python2 python3-docutils xmlto xz-devel \
elfutils-libelf-devel libcap-devel libcap-ng-devel llvm-toolset libyaml libyaml-devel kernel-rpm-macros createrepo \
wget dwarves java-devel libbabeltrace-devel libbpf-devel net-tools python3-devel rsync attr lsof openmpi-devel bc openmpi -y


export PATH=$PATH:/usr/lib64/openmpi/bin/

which mpicc

echo "sleep for 10 seconds....................."
sleep 10
# Checking storage utilization
df -TH && df -iTH

# Downloading kernel packges that are compatible with Lustre version according to Lustre compatibility matrix
echo "........Downloading kernel packges that are compatible with Lustre version according to Lustre compatibility matrix..........."
if [ -d "/root/mirror.centos.org" ]
then
    echo "Directory exists skip download..."
else
    echo "Directory does not exists, download it..."
    cd /root && wget -r -l1 --no-parent -A kernel*4.18.0-240.1.1.*.rpm  http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/
fi

# Checking storage utilization
df -TH && df -iTH

# Downloading e2fsprogs packages for ldiskfs backend
echo "................................Downloading e2fsprogs packages for ldiskfs backend............................."
if [ -d "/root/downloads.whamcloud.com" ]
then
    echo "Directory exists skip download..."
else
    echo "Directory does not exists, download it..."
    cd /root && wget -r -l1 --no-parent -A *1.45.6*.rpm https://downloads.whamcloud.com/public/e2fsprogs/1.45.6.wc5/el8/RPMS/x86_64/
fi

# Checking storage utilization
df -TH && df -iTH

# Checking directory utilization
du -sh /root/*

# Installing some dependency
echo "............................Installing some dependency........................................................"
#cd /root/mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages && yum install kernel-abi-whitelists-4.18.0-240.1.1.el8_3.noarch.rpm -y
cd /root/mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages && yum install *.rpm -y
#cd /root && rm -rf mirror.centos.org

# Installing all e2fsprogs packages
echo ".............................Installing all e2fsprogs packages......................................"
cd /root/downloads.whamcloud.com/public/e2fsprogs/1.45.6.wc5/el8/RPMS/x86_64 && yum install *.rpm -y
#cd /root && rm -rf downloads.whamcloud.com

# Checking storage utilization
df -TH && df -iTH

# Defining PATH were everything will happen
echo "................................Defining PATH were everything will happen.................................."
mkdir /home/devendra
HOME=/home/devendra
cd $HOME

# Pulling git repo which  have lustre source packaige and extracting it.
echo "...........................Pulling git repo which  have lustre source packaige and extracting it........................."
git clone https://github.com/dspatil7768/lustre_packages.git && cd lustre_packages/ && tar -xvf lustre-release-7c0f691.tar.gz && cd lustre-release-7c0f691
#rm -rf $HOME/lustre_packages/lustre-release-7c0f691.tar.gz  

# Checking storage utilization
df -TH && df -iTH

# Checking dependency resolution
echo ".................................Checking dependency resolution............................................"
sh autogen.sh

cd $HOME && mkdir -p kernel/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS} && cd kernel && echo '%_topdir %(echo $HOME)/kernel/rpmbuild' > ~/.rpmmacros


# Downloading kernel source package required for patching 
echo "......................Downloading kernel source package required for patching....................................................."
if [ -f "kernel-4.18.0-240.1.1.el8_3.src.rpm" ]
then
    echo "file exists skip download..."
else
    echo "file does not exists, download it..."
    wget https://vault.centos.org/8.3.2011/BaseOS/Source/SPackages/kernel-4.18.0-240.1.1.el8_3.src.rpm
fi
#wget https://vault.centos.org/8.3.2011/BaseOS/Source/SPackages/kernel-4.18.0-240.1.1.el8_3.src.rpm

# Installing kernel source packages
echo "...........................Installing kernel source packages.................................................."
rpm -ivh kernel-4.18.0-240.1.1.el8_3.src.rpm
rm -rf kernel-4.18.0-240.1.1.el8_3.src.rpm

# Checking storage utilization
df -TH && df -iTH

# Checking directory utilization
echo "########################### Utilization #########################################"
du -sh *

cd $HOME/kernel/rpmbuild/

# Building SPEC file
echo "Building SPEC file..."
rpmbuild -bp --target=`uname -m` ./SPECS/kernel.spec

# Patching starts
echo "######################################################### Patching starts #######################################################################################"
cp ~/kernel/rpmbuild/BUILD/kernel-4.18.0-240.1.1.el8_3/linux-4.18.0-240.1.1.el8.x86_64/configs/kernel-4.18.0-x86_64.config \
~/lustre_packages/lustre-release-7c0f691/lustre/kernel_patches/kernel_configs/kernel-4.18.0-4.18-rhel8.3-x86_64.config

echo "....................Copying of original kernel config file to PATCH directory done........................"

sed -i '440 a CONFIG_IOSCHED_DEADLINE=y\nCONFIG_DEFAULT_IOSCHED="deadline"' ~/lustre_packages/lustre-release-7c0f691/lustre/kernel_patches/kernel_configs/kernel-4.18.0-4.18-rhel8.3-x86_64.config

echo "......................Added IOSCHED parameter in above copied file..................................."

cd $HOME

rm -f $HOME/lustre-kernel-aarch64-lustre.patch

cd $HOME/lustre_packages/lustre-release-7c0f691/lustre/kernel_patches/series

for patch in $(<"4.18-rhel8.series"); do patch_file="$HOME/lustre_packages/lustre-release-7c0f691/lustre/kernel_patches/patches/${patch}"; cat "${patch_file}" >> "$HOME/lustre-kernel-x86_84-lustre.patch"; done

cp $HOME/lustre-kernel-x86_84-lustre.patch ~/kernel/rpmbuild/SOURCES/patch-4.18.0-lustre.patch


# Defining ext4 patching in spec file
echo "..............................Defining ext4 patching in spec file...................................."

sed -i '/>modnames/a  \ \ \ \ cp -a fs\/ext4\/* $RPM_BUILD_ROOT\/lib\/modules\/$KernelVer\/build\/fs\/ext4' $HOME/kernel/rpmbuild/SPECS/kernel.spec

sed -i '/empty final patch to facilitate testing of kernel patches/a  # adds Lustre patches\nPatch99995: patch-%{version}-lustre.patch' $HOME/kernel/rpmbuild/SPECS/kernel.spec 

sed -i '/ApplyOptionalPatch linux-kernel-test.patch/a  # lustre patch\nApplyOptionalPatch patch-%{version}-lustre.patch' $HOME/kernel/rpmbuild/SPECS/kernel.spec

echo '# x86_64' > $HOME/kernel/rpmbuild/SOURCES/kernel-4.18.0-aarch64.config

cat $HOME/lustre_packages/lustre-release-7c0f691/lustre/kernel_patches/kernel_configs/kernel-4.18.0-4.18-rhel8.3-x86_64.config >> $HOME/kernel/rpmbuild/SOURCES/kernel-4.18.0-x86_64.config

cd $HOME/kernel/rpmbuild

buildid="_lustre"

# Checking storage utilization
df -TH && df -iTH

# Checking directory utilization
echo "########################### Utilization Before rpmbuild #########################################"
du -sh *

# Building patched kernel RPM
echo "...................................Building patched kernel RPM....................................................."

echo "################################################### Lustre Building Starts ####################################################"

rpmbuild -ba --with firmware --target x86_64 --with baseonly --without kabichk --define "buildid ${buildid}"  $HOME/kernel/rpmbuild/SPECS/kernel.spec

echo "################################################## rpmbuild performed from SPEC file #########################################"


# Checking storage utilization
df -TH && df -iTH

# Checking directory utilization
echo "########################### Utilization After rpmbuild #########################################"
echo "PATH is:" `pwd` 
du -sh *


#cd $HOME/kernel/rpmbuild/RPMS/x86_64/ && rpm -Uvh kernel-core*.rpm kernel-headers*.rpm kernel-modules*.rpm kernel-modules-extra*.rpm kernel-4.18*.rpm kernel-devel*.rpm kernel-tools*.rpm kernel-tools-libs*.rpm

cd $HOME/kernel/rpmbuild/RPMS/x86_64/ && rpm -Uvh *.rpm

cd $HOME/lustre_packages/lustre-release-7c0f691

# Checking storage utilization
df -TH && df -iTH

# Checking directory utilization
echo "########################### Utilization Before ./configure #########################################"
du -sh *

# Final RPM generation
echo "..........................................Final RPM generation........................................."

./configure --with-linux=$HOME/kernel/rpmbuild/BUILD/kernel-4.18.0-240.1.1.el8_3/linux-4.18.0-240.1.1.el8_lustre.x86_64/ 

# Checking directory utilization
echo "########################### Utilization After ./configure #########################################"
du -sh *


echo "############################## ./configure completed make rpms Starts ##############################" &&  make rpms

# Checking directory utilization
echo "########################### Utilization after make rpms  #########################################"
du -sh *

mkdir $HOME/final_lustre_packages && mv $HOME/lustre_packages/lustre-release-7c0f691/*.rpm $HOME/final_lustre_packages/ && createrepo .
cd $HOME/kernel/rpmbuild/RPMS/x86_64 && mv *.rpm $HOME/final_lustre_packages/

ls -l $HOME/final_lustre_packages/

echo "########################## RPMs Successfully Generated ##########################################"

# Checking storage utilization
df -TH && df -iTH


echo "Deleting and removing unwanted packages to make the docker image minimal..."

cd $HOME && rm -rf kernel
cd /root && rm -rf mirror.centos.org
cd /root && rm -rf downloads.whamcloud.com
yum remove kernel* -y
yum groupremove "Development Tools" -y

yum remove audit-libs-devel binutils-devel elfutils-devel ncurses-devel newt-devel numactl-devel openssl-devel pciutils-devel \
perl perl-devel xz-devel elfutils-libelf-devel libcap-devel libcap-ng-devel libyaml-devel kernel-rpm-macros createrepo \
wget dwarves java-devel libbabeltrace-devel libbpf-devel net-tools python3-devel rsync attr lsof openmpi-devel bc openmpi -y

echo  "Checking storage space after deleting unnecessary packages and downloaded data..."
df -TH

echo "################# Utilization after removing unwanted packages, directories  ############################"
du -sh $HOME/*
